export const theme = {
  surfaceColor: '#000000',
  primaryColor: '#fef854',
  secondaryColor: '#2d44f5',
  borderRadius: '7px',
} as const

export type Theme = typeof theme
