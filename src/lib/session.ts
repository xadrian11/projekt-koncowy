const SESSION_KEY = 'user'

type User = {
  id: string
  name: string
  avatar: string
}

export const setUserData = (data: User) => {
  sessionStorage.setItem(SESSION_KEY, JSON.stringify(data))
}

export const getUserData = () => {
  const userData = sessionStorage.getItem(SESSION_KEY)
  return userData ? JSON.parse(userData) : null
}

export const removeUserData = () => {
  sessionStorage.removeItem(SESSION_KEY)
}
