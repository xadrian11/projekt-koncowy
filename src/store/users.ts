import {
  createSlice,
  createAsyncThunk,
  PayloadAction,
  createSelector,
} from '@reduxjs/toolkit'
import { getUserData, removeUserData, setUserData } from '../lib/session'
import { State } from './store'

export type User = {
  id: string
  name: string
  avatar: string
}

export const userEnter = createAsyncThunk<User, Omit<User, 'id'>>(
  'users/userEnter',
  async (userData) => {
    try {
      const response = await fetch('/api/event', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          type: 'user_enter',
          payload: userData,
        }),
      })
      const user = await response.json()
      setUserData(user)
      return user
    } catch {
      return null
    }
  }
)

export const userExit = createAsyncThunk<User, Omit<User, 'name' | 'avatar'>>(
  'users/userExit',
  async (userId) => {
    try {
      const response = await fetch('/api/event', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          type: 'user_exit',
          payload: userId,
        }),
      })
      removeUserData()
      return await response.json()
    } catch {
      return null
    }
  }
)

const usersSlice = createSlice({
  name: 'users',
  initialState: [] as User[],
  reducers: {
    addUser: (state, action: PayloadAction<User>) => {
      if (!state.find((user) => user.id === action.payload.id)) {
        state.push(action.payload)
      }
    },
    removeUser: (state, action: PayloadAction<User>) => {
      state.splice(
        state.findIndex((user) => user.id === action.payload.id),
        1
      )
    },
    setInitialUsers: (state, action: PayloadAction<User[]>) => {
      return action.payload
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(userEnter.fulfilled, (state, action) => {
        state.push(action.payload)
      })
      .addCase(userExit.fulfilled, (state, action) => {
        state.splice(
          state.findIndex((id) => id === action.payload),
          1
        )
      })
  },
})

export const currentUserSelector = createSelector(
  (state: State) => state.users,
  (users) => users.find((u) => u.id === getUserData()?.id)
)

export default usersSlice
export const { setInitialUsers, addUser, removeUser } = usersSlice.actions
