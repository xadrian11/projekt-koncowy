import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit'

export type Message = {
  id: string
  text: string
  authorId: string
  authorAvatar: string
  authorName: string
  time: string
}

export const sendMessage = createAsyncThunk<Message, Omit<Message, 'id'>>(
  'messages/sendMessage',
  async (message) => {
    try {
      const response = await fetch('/api/event', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          type: 'message',
          payload: message,
        }),
      })
      return await response.json()
    } catch {
      return null
    }
  }
)

const messagesSlice = createSlice({
  name: 'messages',
  initialState: [] as Message[],
  reducers: {
    addMessage: (state, action: PayloadAction<Message>) => {
      if (!state.find((message) => message.id === action.payload.id)) {
        state.push(action.payload)
      }
    },
    setInitialMessages: (state, action: PayloadAction<Message[]>) => {
      return action.payload
    },
  },
  extraReducers: (builder) => {
    builder.addCase(sendMessage.fulfilled, (state, action) => {
      if (!state.find((message) => message.id === action.payload.id)) {
        state.push(action.payload)
      }
    })
  },
})

export default messagesSlice
export const { addMessage, setInitialMessages } = messagesSlice.actions
