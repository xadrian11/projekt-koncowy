import { combineReducers, configureStore } from '@reduxjs/toolkit'
import messagesSlice from './messages'
import usersSlice from './users'

export const store = configureStore({
  reducer: combineReducers({
    [messagesSlice.name]: messagesSlice.reducer,
    [usersSlice.name]: usersSlice.reducer,
  }),
})

export type Dispatch = typeof store.dispatch
export type State = ReturnType<typeof store.getState>
