import styled from 'styled-components'

export const ContenerNotification = styled.article`
  display: flex;
  flex-direction: column;
  align-items: center;
  border: 3px solid #ffeeff;
  border-radius: 7px;
  background-color: ${(props) => props.theme.surfaceColor};
  border: solid ${(props) => props.theme.secondaryColor} 2px;
  color: ${(props) => props.theme.primaryColor};
  margin: 15px;
  padding: 0.5rem 0;
  @media (min-width: 280px) and (max-width: 660px) {
    margin: 0 7px;
  }
`

export const OnlineInformation = styled.div`
  font-size: 0.92rem;
  color: ${(props) => props.theme.primaryColor};
`
export const InformationList = styled.div`
  color: ${(props) => props.theme.primaryColor};
  font-size: 0.85rem;
  padding-bottom: 0.5rem;
`

export const NotificationContainer = styled.div`
  padding: 0.25rem;
`
