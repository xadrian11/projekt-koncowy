import React from 'react'
import {
  ContenerNotification,
  OnlineInformation,
  InformationList,
  NotificationContainer,
} from './CurrentUserNotification.styled'
import { useSelector } from 'react-redux'
import { State } from '../../store'

const CurrentUserNotification = () => {
  const users = useSelector((state: State) => state.users)
  const newestUser = users[users.length - 1]

  return (
    <ContenerNotification>
      <OnlineInformation>
        {users.length > 1 && (
          <InformationList>{users.length} users online now</InformationList>
        )}
        {users.length <= 1 && (
          <InformationList>{users.length} user online now</InformationList>
        )}
      </OnlineInformation>
      {newestUser && (
        <NotificationContainer>
          {newestUser.name} just joined the chat
        </NotificationContainer>
      )}
    </ContenerNotification>
  )
}

export default CurrentUserNotification
