import styled from 'styled-components'

export const OwnMessageWrapper = styled.div`
  padding: 1rem;
  :hover {
    transform: translateX(-40px);
  }
  transition: all 0.5s ease;
  margin-left: auto;
  display: inline-block;
  max-width: 90%;
  color: ${(props) => props.theme.primaryColor};
`

export const OwnMessageItem = styled.div`
  position: relative;
  display: flex;
  justify-content: flex-end;
`
export const OwnMessageText = styled.div`
  width: 100%;
  padding: 10px;
  border-radius: 10px;
  font-size: 20px;
  background-color: ${(props) => props.theme.surfaceColor};
  border: dotted ${(props) => props.theme.primaryColor} 5px;
  color: ${(props) => props.theme.primaryColor};
  z-index: 2;
  word-break: break-all;
`
export const OwnTime = styled.p`
  position: absolute;
  top: 25%;
  right: 0%;
  transition: all 0.5s ease;
  ${OwnMessageWrapper}:hover & {
    transform: translateX(45px);
  }
`

export const OwnName = styled.p`
  margin: 0 5px;
  font-size: 16px;
  text-align: right;
  color: ${(props) => props.theme.primaryColor};
`

export const Avatar = styled.img`
  border-radius: 100%;
  height: 50px;
  margin: 0 10px;
`

export const MessageWrapper = styled(OwnMessageWrapper)`
  :hover {
    transform: translateX(40px);
  }
  margin-left: 0;
`

export const MessageItem = styled(OwnMessageItem)`
  justify-content: flex-start;
`

export const MessageText = styled(OwnMessageText)`
  background-color: ${(props) => props.theme.surfaceColor};
  border: solid ${(props) => props.theme.secondaryColor} 2px;
  color: ${(props) => props.theme.primaryColor};
  width: auto;
`

export const Name = styled(OwnName)`
  text-align: left;
`

export const Time = styled(OwnTime)`
  left: 0;
  ${MessageWrapper}:hover & {
    transform: translateX(-45px);
  }
`
