import {
  MessageItem,
  MessageText,
  MessageWrapper,
  OwnMessageText,
  OwnMessageItem,
  OwnMessageWrapper,
  OwnName,
  Name,
  Time,
  OwnTime,
  Avatar,
} from './Message.styled'

type MessageProps = {
  name: string
  time: string
  text: string
  own?: boolean
  avatar: string
}

export function Message({ text, name, own, time, avatar }: MessageProps) {
  if (own) {
    return (
      <>
        <OwnMessageWrapper>
          <OwnMessageItem>
            <Avatar src={avatar} />
            <OwnMessageText>{text}</OwnMessageText>
            <OwnTime>{time}</OwnTime>
          </OwnMessageItem>
          <OwnName>{name}</OwnName>
        </OwnMessageWrapper>
      </>
    )
  } else {
    return (
      <MessageWrapper>
        <MessageItem>
          <Time>{time}</Time>
          <MessageText>{text}</MessageText>
          <Avatar src={avatar} />
        </MessageItem>
        <Name>{name}</Name>
      </MessageWrapper>
    )
  }
}
