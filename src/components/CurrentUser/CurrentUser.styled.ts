import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  justify-content: center;
  margin: 0 0.75rem auto;
  max-height: 500px;

  @media (min-width: 420px) and (max-width: 660px) {
    justify-content: center;
    padding-bottom: 20%;
  }
`

export const UserSection = styled.section`
  background-color: ${(props) => props.theme.surfaceColor};
  border: solid ${(props) => props.theme.secondaryColor} 2px;
  border-radius: ${(props) => props.theme.borderRadius};
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  height: 100%;

  @media (min-width: 420px) and (max-width: 660px) {
    margin: 10px 30px;
    min-width: 280px;
  }
`

export const ChatSection = styled.section`
  background-color: ${(props) => props.theme.surfaceColor};
  display: flex;
  flex-direction: column;

  @media (min-width: 420px) and (max-width: 660px) {
    margin: 24px 17px;
    height: 100%;
    width: 120%;
  }
`

export const Wrapper = styled.div`
  background-color: ${(props) => props.theme.surfaceColor};
  display: flex;
  flex-basis: 500px;
`
