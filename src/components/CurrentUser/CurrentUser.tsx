import React from 'react'
import { UserSection, Container } from './CurrentUser.styled'
import CurrentUserAvatar from '../CurrentUserAvatar/CurrentUserAvatar'
import CurrentUserNotification from '../CurrentUserNotification/CurrentUserNotification'

const CurrentUser = () => {
  return (
    <Container>
      <UserSection>
        <CurrentUserAvatar />
        <CurrentUserNotification />
      </UserSection>
    </Container>
  )
}

export default CurrentUser
