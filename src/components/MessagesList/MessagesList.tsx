import React, { useEffect, useRef } from 'react'
import { Wrapper } from './MessagesList.styled'
import { State } from '../../store'
import { useSelector } from 'react-redux'
import { Message } from '../Message'
import { currentUserSelector } from '../../store/users'

export const MessagesList = () => {
  const messages = useSelector((state: State) => state.messages)
  const currentUser = useSelector(currentUserSelector)
  const wrapperRef = useRef<HTMLDivElement | null>(null)
  useEffect(() => {
    if (wrapperRef.current) {
      wrapperRef.current.scrollTop = wrapperRef.current.scrollHeight
    }
  }, [messages.length])

  return (
    <Wrapper ref={wrapperRef}>
      {currentUser &&
        messages.map((message) => {
          return (
            <Message
              key={message.id}
              text={message.text}
              own={currentUser.id === message.authorId}
              name={message.authorName}
              time={message.time}
              avatar={message.authorAvatar}
            />
          )
        })}
    </Wrapper>
  )
}
