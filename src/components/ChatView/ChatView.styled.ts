import styled from 'styled-components'

export const DesktopChatContainer = styled.div`
  display: flex;
  justify-content: space-evenly;
  height: 80%;
  width: 80%;
  background-color: ${(props) => props.theme.surfaceColor};
  border: solid ${(props) => props.theme.secondaryColor} 5px;
  border-radius: ${(props) => props.theme.borderRadius};
  padding: 30px;
`

export const MobileChatContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 85%;
  width: 90%;
  background-color: ${(props) => props.theme.surfaceColor};
  border-radius: ${(props) => props.theme.borderRadius};
  padding: 10px;
  gap: 10px;
`

export const MessagesContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
  flex-grow: 1;
`
