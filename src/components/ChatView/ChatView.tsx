import { useEffect, useState, useMemo } from 'react'
import {
  MessagesContainer,
  MobileChatContainer,
  DesktopChatContainer,
} from './ChatView.styled'
import { MessagesList } from '../MessagesList'
import { MessageComposer } from '../MessageComposer'
import CurrentUser from '../CurrentUser/CurrentUser'

export function ChatView() {
  const isMobile = useMediaQuery('(max-width: 768px)')

  function useMediaQuery(query: string) {
    const media = useMemo(() => window.matchMedia(query), [query])
    const [matches, setMatches] = useState(media.matches)

    useEffect(() => {
      setMatches(media.matches)
      const listener = (event: MediaQueryListEvent) => setMatches(event.matches)
      media.addEventListener('change', listener)
      return () => media.removeEventListener('change', listener)
    }, [media])

    return matches
  }

  const ChatContainer = isMobile ? MobileChatContainer : DesktopChatContainer

  return (
    <>
      <img src="/chat-logo.png" alt="logo" height="35px" width="210px" />
      <ChatContainer>
        <CurrentUser />
        <MessagesContainer>
          <MessagesList />
          <MessageComposer />
        </MessagesContainer>
      </ChatContainer>
    </>
  )
}
