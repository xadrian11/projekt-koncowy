import React from 'react'
import { useState } from 'react'
import { Dispatch } from '../../store'
import { useDispatch, useSelector } from 'react-redux'
import { sendMessage } from '../../store/messages'
import { Button, Input } from './MessageComposer.styled'
import { currentUserSelector } from '../../store/users'

export function MessageComposer() {
  const [messageText, setMessageText] = useState('')
  const dispatch = useDispatch<Dispatch>()
  const currentUser = useSelector(currentUserSelector)

  const isDisabled = messageText.length === 0

  const maxLength = 1000

  const handleTypeText = (e: React.ChangeEvent<HTMLInputElement>) => {
    setMessageText(e.target.value)
  }

  const getCurrentTime = () =>
    new Date().toLocaleTimeString([], {
      hour: '2-digit',
      minute: '2-digit',
    })

  function handleSendMessage(event: React.FormEvent) {
    if (currentUser) {
      event.preventDefault()
      setMessageText('')
      dispatch(
        sendMessage({
          text: messageText,
          authorId: currentUser.id,
          authorName: currentUser.name,
          authorAvatar: currentUser.avatar,
          time: getCurrentTime(),
        })
      )
    }
  }

  return (
    <form onSubmit={handleSendMessage}>
      <Input
        value={messageText}
        onChange={handleTypeText}
        maxLength={maxLength}
      />
      <Button type="submit" disabled={isDisabled}>
        {'Send'}
      </Button>
    </form>
  )
}
