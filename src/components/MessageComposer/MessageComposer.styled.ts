import styled, { css } from 'styled-components'

export const Button = styled.button`
  background-color: ${(props) => props.theme.surfaceColor};
  border: solid ${(props) => props.theme.secondaryColor} 2px;
  color: ${(props) => props.theme.primaryColor};
  height: 30px;
  width: 60px;
  border-radius: 5px;
  cursor: pointer;

  ${(props) =>
    props.disabled &&
    css`
      cursor: not-allowed;
      color: ${(props) => props.theme.secondaryColor};
    `}
`

export const Input = styled.input`
  background-color: ${(props) => props.theme.surfaceColor};
  border: solid ${(props) => props.theme.secondaryColor} 2px;
  color: ${(props) => props.theme.primaryColor};
  border-radius: ${(props) => props.theme.borderRadius};
  height: 30px;
  border-radius: 5px;
  padding-left: 5px;
  width: calc(100% - 60px);
  outline: none;
`
