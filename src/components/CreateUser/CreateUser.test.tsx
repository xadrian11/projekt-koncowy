import CreateUser from './CreateUser'
import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { Provider } from 'react-redux'
import { store } from '../../store'

describe('CreateUser.tsx', () => {
  it('should enable button when username is set', async () => {
    render(
      <Provider store={store}>
        <CreateUser />
      </Provider>
    )

    const input = screen.getByRole('textbox')
    const button = screen.getByText('Confirm')

    expect(button.hasAttribute('disabled')).toBeTruthy()

    await userEvent.type(input, 'name')

    expect(button.hasAttribute('disabled')).toBeFalsy()
  })
})
