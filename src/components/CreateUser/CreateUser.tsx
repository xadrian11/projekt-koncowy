import React, { useState } from 'react'
import { Dispatch } from '../../store'
import { useDispatch } from 'react-redux'
import { userEnter } from '../../store/users'
import {
  Wrapper,
  AvatarWrapper,
  Avatar,
  Button,
  Input,
  Form,
  IconButton,
} from './CreateUser.styled'
import { FaArrowAltCircleRight, FaArrowAltCircleLeft } from 'react-icons/fa'

function CreateUser() {
  const [avatar, setAvatar] = useState(() => {
    const avatarIndex = Math.floor(Math.random() * 8) + 1
    return `/avatars/avataaars_${avatarIndex}.png`
  })
  const [username, setUsername] = useState('')

  const dispatch = useDispatch<Dispatch>()

  const avatarEndIndex = 8
  const avatarCurrentIndex = +avatar.charAt(avatar.length - 5)

  const isDisabled = username.length === 0

  const handleSubmit = (event: React.FormEvent) => {
    event.preventDefault()
    dispatch(userEnter({ name: username, avatar }))
  }

  const prevAvatar = () => {
    const prevIndex =
      avatarCurrentIndex === 1 ? avatarEndIndex : avatarCurrentIndex - 1
    setAvatar(`/avatars/avataaars_${prevIndex}.png`)
  }
  const nextAvatar = () => {
    const nextIndex =
      avatarCurrentIndex === avatarEndIndex ? 0 : avatarCurrentIndex + 1
    setAvatar(`/avatars/avataaars_${nextIndex}.png`)
  }

  return (
    <Wrapper>
      <AvatarWrapper>
        <IconButton>
          <FaArrowAltCircleLeft onClick={prevAvatar} />
        </IconButton>
        <Avatar alt="avatar" src={avatar} />
        <IconButton>
          <FaArrowAltCircleRight onClick={nextAvatar} />
        </IconButton>
      </AvatarWrapper>
      <h2>Choose your avatar</h2>
      <Form onSubmit={handleSubmit}>
        <Input
          value={username}
          maxLength={16}
          placeholder="Enter your name"
          onChange={(e) => {
            setUsername(e.target.value)
          }}
        ></Input>
      </Form>
      <Button type="submit" onClick={handleSubmit} disabled={isDisabled}>
        Confirm
      </Button>
    </Wrapper>
  )
}

export default CreateUser
