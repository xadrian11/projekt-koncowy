import styled, { css } from 'styled-components'

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: 1rem;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100vh;
  background-color: ${(props) => props.theme.surfaceColor};
`
export const Button = styled.button`
  background-color: ${(props) => props.theme.surfaceColor};
  border: solid ${(props) => props.theme.secondaryColor} 2px;
  color: ${(props) => props.theme.primaryColor};
  border-radius: 1rem;
  font-size: 2rem;
  padding: 1rem;
  width: 15rem;
  transition: all 0.5s;
  cursor: pointer;
  box-shadow: 0 1rem 2rem -1rem rgba(0, 0, 0, 0.7);
  position: relative;
  transition: 0.5s;

  &:after {
    content: '»';
    position: absolute;
    opacity: 0;
    top: 1rem;
    right: 0;
    transition: 0.5s;
  }

  &:hover {
    padding-right: 1.5rem;
    padding-left: 0.5rem;
  }

  &:hover:after {
    opacity: 1;
    right: 1.75rem;
  }
  ${(props) =>
    props.disabled &&
    css`
      cursor: not-allowed;
      color: ${(props) => props.theme.secondaryColor};
    `}
`
export const Input = styled.input`
  appearance: none;
  background-color: white;
  padding: 1rem;
  border-radius: 1rem;
  width: 20ch;
  font-size: 2rem;
  text-align: center;
  box-shadow: 0 1rem 2rem -1rem rgba(0, 0, 0, 0.7);
`

export const Form = styled.form`
  display: flex;
  flex-direction: column;
  gap: 1rem;
`

export const AvatarWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`

export const IconButton = styled.button`
  font-size: 3rem;
  color: #e9eaec;
  cursor: pointer;
  background: none;
  border: none;
  border-radius: 50%;
  transition: transform 0.2s ease-in-out;
  &:hover {
    transform: scale(1.2);
  }
`
export const Avatar = styled.img`
  border-radius: 45% 45% 50% 50%;
  box-shadow: 0 1rem 2rem -1rem rgba(0, 0, 0, 0.7);
  user-select: none;
`
