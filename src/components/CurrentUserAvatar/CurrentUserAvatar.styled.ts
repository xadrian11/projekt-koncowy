import styled from 'styled-components'

export const Avatar = styled.div`
  min-width: 250px;
  height: 280px;
  background-color: ${(props) => props.theme.surfaceColor};
  border: solid ${(props) => props.theme.secondaryColor} 2px;
  border-radius: 7px;
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  flex-wrap: wrap;
  align-content: center;
  margin-bottom: 7px;
  margin: 15px;
  @media (min-width: 280px) and (max-width: 660px) {
    max-width: 266px;
    margin: 22px 10px;
  }
`

export const ImageAvatar = styled.img`
  height: 170px;
  @media (min-width: 280px) and (max-width: 660px) {
    width: 250px;
  }
`
export const Greeting = styled.h2`
  background-color: ${(props) => props.theme.surfaceColor};
  font-size: 0.9rem;
  font-weight: bold;
  color: ${(props) => props.theme.primaryColor};
`

export const SignOutBtn = styled.button`
  font-size: 1rem;
  font-weight: bold;
  padding: 9px;
  border: solid ${(props) => props.theme.secondaryColor} 2px;
  border-radius: 7px;
  font-weight: bold;
  background-color: ${(props) => props.theme.surfaceColor};
  color: ${(props) => props.theme.primaryColor};
  cursor: pointer;
  margin-top: 0.75rem;
`

export const Info = styled.div`
  display: flex;
  height: 20%;
`
