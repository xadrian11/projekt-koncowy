import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { currentUserSelector, userExit } from '../../store/users'
import { Dispatch } from '../../store'
import {
  Avatar,
  ImageAvatar,
  Greeting,
  SignOutBtn,
} from './CurrentUserAvatar.styled'

const CurrentUserAvatar = () => {
  const currentUser = useSelector(currentUserSelector)
  const dispatch = useDispatch<Dispatch>()

  const handleUserExit = () => {
    if (currentUser) {
      dispatch(userExit({ id: currentUser.id }))
    }
  }

  return (
    <Avatar>
      <ImageAvatar src={currentUser?.avatar} alt="currentUserAvatar" />
      <Greeting>Hello, {currentUser?.name}!</Greeting>
      <SignOutBtn onClick={handleUserExit}>Sign out</SignOutBtn>
    </Avatar>
  )
}

export default CurrentUserAvatar
