import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { pusher } from '../../lib/pusher'
import { addMessage, Message, setInitialMessages } from '../../store/messages'
import {
  addUser,
  currentUserSelector,
  removeUser,
  setInitialUsers,
  User,
} from '../../store/users'
import { Dispatch, State } from '../../store'
import CreateUser from '../CreateUser/CreateUser'
import { AppContainer } from './App.styled'
import { ChatView } from '../ChatView'

export function App() {
  const dispatch = useDispatch<Dispatch>()
  const [isLoading, setIsLoading] = useState(true)
  const currentUser = useSelector(currentUserSelector)

  const getChatData = async (): Promise<State | null> => {
    try {
      const data = await fetch('api/chat')
      return data.json()
    } catch (error) {
      return null
    }
  }

  useEffect(() => {
    getChatData().then((chat) => {
      if (chat) {
        dispatch(setInitialMessages(chat.messages))
        dispatch(setInitialUsers(chat.users))
        setIsLoading(false)
      }
    })
  }, [dispatch])

  useEffect(() => {
    const chat = pusher.subscribe('chat')
    const messageHandler = (message: Message) => {
      dispatch(addMessage(message))
    }
    const userEnterHandler = (user: User) => {
      dispatch(addUser(user))
    }
    const userExitHandler = (user: User) => {
      dispatch(removeUser(user))
    }

    chat.bind('message', messageHandler)
    chat.bind('user_enter', userEnterHandler)
    chat.bind('user_exit', userExitHandler)

    return () => {
      chat.unbind('message', messageHandler)
      chat.unbind('user_enter', userEnterHandler)
      chat.unbind('user_exit', userExitHandler)
      chat.unsubscribe()
    }
  }, [dispatch])

  if (isLoading) {
    return null
  }

  return (
    <>
      {currentUser ? (
        <AppContainer>
          <ChatView />
        </AppContainer>
      ) : (
        <CreateUser />
      )}
    </>
  )
}
