import styled from 'styled-components'

export const AppContainer = styled.div`
  background-color: ${(props) => props.theme.surfaceColor};
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  @media (max-width: 768px) {
    min-height: 100vh;
    height: auto;
  }
`
