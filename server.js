const Pusher = require('pusher')
const { json } = require('micro')
const { v4 } = require('uuid')

const chat = {
  users: [],
  messages: [],
}

const pusher = new Pusher({
  appId: '1405297',
  key: '82e41f059c832a331344',
  secret: '06b666d39adee2248836',
  cluster: 'eu',
  useTLS: true,
})

module.exports = async (req, res) => {
  if (req.url === '/api/chat' && req.method === 'GET') {
    res.end(JSON.stringify(chat))
    return
  }

  if (req.url === '/api/event' && req.method === 'POST') {
    const { type, payload } = await json(req)

    if (type === 'message') {
      const message = { ...payload, id: v4() }
      await pusher.trigger('chat', 'message', message)
      chat.messages.push(message)
      res.end(JSON.stringify(message))
      return
    }

    if (type === 'user_enter') {
      const user = { ...payload, id: v4() }
      await pusher.trigger('chat', 'user_enter', user)
      chat.users.push(user)
      res.end(JSON.stringify(user))
      return
    }

    if (type === 'user_exit') {
      chat.users = chat.users.filter((u) => u.id !== payload.id)
      await pusher.trigger('chat', 'user_exit', payload)
      res.end(JSON.stringify({ id: payload.id }))
      return
    }

    res.end('Not Found')
    return
  }

  res.end('Not Found')
}
