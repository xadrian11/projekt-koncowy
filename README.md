## Chat App

![Logo](https://gitlab.com/xadrian11/project-doc/-/raw/6d27dab91f0a94c7ffd8c19c2f953b3e6637130f/image/chat-logo.png)

## Technologies

![Logo](https://gitlab.com/xadrian11/project-doc/-/raw/main/image/stack.PNG)

## Chat View

![Chat View](https://gitlab.com/xadrian11/project-doc/-/raw/main/image/widok.PNG)

## Create UserView

![Create UserView](https://gitlab.com/xadrian11/project-doc/-/raw/main/image/wybor.PNG)

## ⚙️ How to run a project

### Clone repository

```sh
git clone https://gitlab.com/xadrian11/projekt-koncowy.git
```

### Install dependecies

```sh
npm install
```

### Start dev server

```sh
npm run dev
```

### Run tests

```sh
npm run test
```

The app is availalbe under `http://localhost:3000` and the API under `http://localhost:3000/api`
