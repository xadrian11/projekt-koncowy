import { defineConfig } from 'vitest/config'
import react from '@vitejs/plugin-react'

export default defineConfig({
  plugins: [react()],
  test: {
    environment: 'happy-dom',
  },
  server: {
    proxy: {
      '^/api': {
        changeOrigin: true,
        target: 'http://127.0.0.1:5555',
      },
    },
  },
})
